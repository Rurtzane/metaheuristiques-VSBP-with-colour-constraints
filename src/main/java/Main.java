import algoGenetique.*;
import algoTabou.Export;
import algoTabou.Tabou;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static algoTabou.Tabou.recherche;

public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {

		if(args[0].equals("genetique")) {
			GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm();
			List<BoxGenetic> boxesSrc = new ArrayList<>();
			List<ItemGenetic> itemsSrc = new ArrayList<>();

			if(args.length == 1) {
				System.out.println(StringUtilsAlgoGenetique.NO_NAMEFILE);
				return;
			}

			try {
				String fileName = args[1];
				FileReaderGenetic.readData(itemsSrc, boxesSrc, fileName);
			}catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			Solution bestSolution = geneticAlgorithm.execute(boxesSrc, itemsSrc, ParamAlgoGenetique.GENERATION_NUMBER, ParamAlgoGenetique.SOLUTIONS_AT_GENERATE);

			if(bestSolution != null) {
				System.out.println();
				System.out.println(StringUtilsAlgoGenetique.LOST_SPACE + bestSolution.getLostSpace());
			} else {
				System.out.println(StringUtilsAlgoGenetique.NO_SOLUTION);
			}
		} else if(args[0].equals("tabou")) {
			final String bench = args[1];
			final int tabu = Integer.parseInt(args[2]);
			final int maxIter = Integer.parseInt(args[3]);
			final int nbEloignement = Integer.parseInt(args[4]);
			
			System.out.println(Tabou.recherche(tabu, maxIter, nbEloignement, bench));
		}
	}

}
