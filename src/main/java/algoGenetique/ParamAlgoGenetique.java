package algoGenetique;

public interface ParamAlgoGenetique {
    int GENERATION_NUMBER = 50;
    int SOLUTIONS_AT_GENERATE = 120;
    int MAX_ACCEPT_BEFORE_AUTOMATIC_FILLING = 3; //  In the "fillBox" function, he will try to fill a box with randomly drawn items. After a while, most of the items are linked to boxes. Therefore by randomly pulling items, it will fall back on the same items. Except that these items do not necessarily respect the remaining capacity of the box or colours. To avoid this problem, we accept a number of errors before manually pulling the item that can fill the remaining capacity of the box.
    int MAX_ACCEPT_BEFORE_AUTOMATIC_SEARCH_BOX = 2;
    CrossoverType CROSSOVER_TYPE = CrossoverType.BEST_SOLUTION;
}
