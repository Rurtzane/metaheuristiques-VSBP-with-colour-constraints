package algoGenetique;

public enum CrossoverType {
    BASIC,
    BEST_SOLUTION,
    BEST_WITH_BAD;
}
