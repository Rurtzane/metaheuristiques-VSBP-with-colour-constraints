package algoGenetique;

public interface StringUtilsAlgoGenetique {

    String KEY_CONTROL_ITEMS_LINKED = "allItemsNotUse";
    String ALL_ITEMS_LINKED_KO = "All items linked KO";
    String ALL_ITEMS_LINKED_OK = "All items linked OK";

    String KEY_CONTROL_COLOR_BOX = "constraintColorNotRespected";
    String CONSTRAINTS_BOX_COLORS_KO = "Constraints box colors KO";
    String CONSTRAINTS_BOX_COLORS_OK = "Constraints box colors OK";

    String GENERATION = "Generation : ";
    String LOST_SPACE = "lost space : ";
    String NO_SOLUTION = "No solution because the constraints not respected";
    String NO_NAMEFILE = "Please, enter the filename in argument";

}
