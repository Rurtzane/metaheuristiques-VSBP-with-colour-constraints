package algoGenetique;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {

		GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm();
		List<BoxGenetic> boxesSrc = new ArrayList<>();
		List<ItemGenetic> itemsSrc = new ArrayList<>();

		if(args.length == 0) {
			System.out.println(StringUtilsAlgoGenetique.NO_NAMEFILE);
			return;
		}

		try {
			String fileName = args[0];
			FileReaderGenetic.readData(itemsSrc, boxesSrc, fileName);
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		Solution bestSolution = geneticAlgorithm.execute(boxesSrc, itemsSrc, ParamAlgoGenetique.GENERATION_NUMBER, ParamAlgoGenetique.SOLUTIONS_AT_GENERATE);

		if(bestSolution != null) {
			System.out.println();
			System.out.println(StringUtilsAlgoGenetique.LOST_SPACE + bestSolution.getLostSpace());
		} else {
			System.out.println(StringUtilsAlgoGenetique.NO_SOLUTION);
		}
	}

}
