package algoGenetique;

import java.util.*;

public class BoxGenetic {
	private final int MAX_COLORS = 2;

	private int capacity;
	private int leftCapacity;
	private List<Integer> colors;
	private Map<Integer, ItemGenetic> items;

	public BoxGenetic() {
		colors = new ArrayList<>();
		items = new HashMap<>();
	}

    public BoxGenetic(BoxGenetic box) {
        this.capacity = box.getCapacity();
        this.leftCapacity = box.getLeftCapacity();
		this.colors = new ArrayList<>();
		this.items = new HashMap<>();
    }

	/**
	 * Add an item to the box
	 * @param item item
	 */
	public void addItem(ItemGenetic item) {
		items.put(item.getId(), item);
	}

	/**
	 * Decrease the left capacity of the box with the capcity of item
	 * @param item Item
	 */
	public void decreaseLeftCapacity(ItemGenetic item) {
		this.leftCapacity -= item.getWeight();
	}

	/**
	 * Add the color to the box
	 * @param item Item
	 */
	public void addColor(ItemGenetic item) {
		colors.add(item.getColor());
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getLeftCapacity() {
		return leftCapacity;
	}
	
	public void setLeftCapacity(int leftCapacity) {
		this.leftCapacity = leftCapacity;
	}
	
	public List<Integer> getColors() {
		return colors;
	}
	
	public void setColors(List<Integer> colors) {
		this.colors = colors;
	}
	
	public Map<Integer, ItemGenetic> getItems() {
		return items;
	}

	public void setItems(Map<Integer, ItemGenetic> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Total capacity : " + capacity + "\nLeft capacity : " + leftCapacity 
				+ "\nNumber of items : " + items.size() +  "\nItems : \n" + items + "\n\n";
	}

    @Override
    public boolean equals(Object obj) {
        BoxGenetic box = (BoxGenetic) obj;

        return capacity == box.getCapacity() && items.equals(box.getItems());
    }

    public boolean equalsBox(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoxGenetic box = (BoxGenetic) o;
        return  capacity == box.capacity &&
				leftCapacity == box.leftCapacity &&
                Objects.equals(colors, box.colors) &&
                Objects.equals(items, box.items);
    }
}
