package algoGenetique;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Solution implements Comparable<Solution> {

    private List<BoxGenetic> boxes;
    private Integer capacityMax;
    private Integer lostSpace;

    public Solution() {
        boxes = new ArrayList<>();
        capacityMax = 0;
        lostSpace = 0;
    }

    public void addBox(BoxGenetic box) {
        boxes.add(box);
    }

    @Override
    public int compareTo(Solution solution) {
        return Comparator.comparing(Solution::getLostSpace)
                .thenComparing(Solution::getCapacityMax)
                .compare(this, solution);
    }

    public List<BoxGenetic> getBoxes() {
        return boxes;
    }

    public void setBoxes(List<BoxGenetic> boxes) {
        this.boxes = boxes;
    }

    public int getCapacityMax() {
        return capacityMax;
    }

    public void setCapacityMax(int capacityMax) {
        this.capacityMax = capacityMax;
    }

    public Integer getLostSpace() {
        return lostSpace;
    }

    public void setLostSpace(Integer lostSpace) {
        this.lostSpace = lostSpace;
    }
}
