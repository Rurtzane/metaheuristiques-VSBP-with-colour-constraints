package algoGenetique;

import java.util.*;
import java.util.List;

public class GeneticAlgorithm {

    /**
     * Execute the algorithm Genetic algorithm
     * @param boxesSrc boxes src
     * @param itemsSrc items src
     * @param nbGeneration nb of generation
     * @param nbSolutionAtGenerate nb of solution at generate
     * @return the best solution
     */
    public Solution execute(final List<BoxGenetic> boxesSrc, final List<ItemGenetic> itemsSrc, final int nbGeneration, final int nbSolutionAtGenerate) {
        List<Solution> solutions = genesis(boxesSrc, itemsSrc, nbSolutionAtGenerate);
        Solution bestSolutionOfGeneration = solutions.get(0);
        System.out.println();
        System.out.println(StringUtilsAlgoGenetique.GENERATION + "0 , " + StringUtilsAlgoGenetique.LOST_SPACE + " " +bestSolutionOfGeneration.getLostSpace());
        for(int i=1; i <= nbGeneration; i++) {
            solutions = selection(solutions);
            mutation(solutions, itemsSrc, boxesSrc);
            bestSolutionOfGeneration = solutions.get(0);
            System.out.println(StringUtilsAlgoGenetique.GENERATION + i + " , " + StringUtilsAlgoGenetique.LOST_SPACE + " " +bestSolutionOfGeneration.getLostSpace());
        }
        System.out.println();
        Map<String, Boolean> controls = controlConstraintsSolution(solutions, itemsSrc.size());
        displayControl(controls, StringUtilsAlgoGenetique.KEY_CONTROL_COLOR_BOX, StringUtilsAlgoGenetique.CONSTRAINTS_BOX_COLORS_KO, StringUtilsAlgoGenetique.CONSTRAINTS_BOX_COLORS_OK);
        displayControl(controls, StringUtilsAlgoGenetique.KEY_CONTROL_ITEMS_LINKED, StringUtilsAlgoGenetique.ALL_ITEMS_LINKED_KO, StringUtilsAlgoGenetique.ALL_ITEMS_LINKED_OK);
        boolean noProblem = !controls.get(StringUtilsAlgoGenetique.KEY_CONTROL_COLOR_BOX) && !controls.get(StringUtilsAlgoGenetique.KEY_CONTROL_ITEMS_LINKED);
        if(noProblem) {
            bestSolutionOfGeneration = solutions.get(0);
            return bestSolutionOfGeneration;
        } else {
            return null;
        }
    }

    /**
     * Check that all the constraints of the problem are respected
     * @param solutions the generation
     * @param sizeItem the item number src
     * @return A map of control name (key) with value boolean for know if the control is respected
     */
    private Map<String, Boolean> controlConstraintsSolution(List<Solution> solutions, int sizeItem) {
        Map<String, Boolean> controls = new HashMap<>();
        controls.put(StringUtilsAlgoGenetique.KEY_CONTROL_COLOR_BOX, false);
        controls.put(StringUtilsAlgoGenetique.KEY_CONTROL_ITEMS_LINKED, false);
        checkConstraints:
        for(Solution solution : solutions) {
            List<Integer> idItems = new ArrayList<>();
            for(BoxGenetic box : solution.getBoxes()) {
                if(box.getColors().size() > 2) {
                    controls.put(StringUtilsAlgoGenetique.KEY_CONTROL_COLOR_BOX, true);
                    break checkConstraints;
                }
                for(Map.Entry<Integer, ItemGenetic> entry : box.getItems().entrySet()) {
                    if(!idItems.contains(entry.getValue().getId())) {
                        idItems.add(entry.getValue().getId());
                    }
                }
            }
            if(idItems.size() != sizeItem) {
                controls.put(StringUtilsAlgoGenetique.KEY_CONTROL_ITEMS_LINKED, true);
                break;
            }
        }
        return controls;
    }

    /**
     * Display a message in the console to see if it's ok or ko
     * @param controls Map<String, Boolean>
     * @param controlVerified the control at verified
     * @param ok the msg if the control is ok
     * @param ko the msg if the control is ko
     */
    private void displayControl(Map<String, Boolean> controls, String controlVerified, String ko, String ok) {
        if (controls.get(controlVerified)) {
            System.out.println(ko);
        } else {
            System.out.println(ok);
        }
    }

    /**
     * Generate first generation randomly
     * @param boxesSrc List<Box source
     * @param itemsSrc List<Item> source
     * @param nbSolution nb solution at generate
     * @return List<Solution> with size of nbSolution (first generation)
     */
    private List<Solution> genesis(final List<BoxGenetic> boxesSrc, final List<ItemGenetic> itemsSrc, final int nbSolution) {
        List<Solution> solutions = new ArrayList<>();
        Random random = new Random();
        for(int idxSolution = 0; idxSolution < nbSolution; idxSolution++) {
            Solution solution = new Solution();
            boolean allItemNotLinked = false;
            // clone the list items
            List<ItemGenetic> itemsSrcClone = copyItems(itemsSrc);
            // while all items are not linked
            while(!allItemNotLinked) {
                // create a box and fill it
                int randomIdxBox = random.nextInt(boxesSrc.size());
                BoxGenetic randomBox = new BoxGenetic(boxesSrc.get(randomIdxBox));
                fillBox(randomBox, itemsSrcClone);
                // add box in the solution
                solution.addBox(randomBox);
                // check if all item are linked at boxes of the solution
                allItemNotLinked = checkAllItemLinked(itemsSrcClone);
            }
            // add solution in list solutions
            solutions.add(solution);
        }
        refreshSolutions(solutions);
        return solutions;
    }

    /**
     * Copy items in the new list and return this list
     * @param items items
     * @return an new list with items copy
     */
    private List<ItemGenetic> copyItems(List<ItemGenetic> items) {
        List<ItemGenetic> itemsSrcClone = new ArrayList<>();
        for (ItemGenetic item : items) {
            itemsSrcClone.add(new ItemGenetic(item));
        }
        return itemsSrcClone;
    }

    /**
     * Is all items linked with boxes
     * @param items list of items
     * @return true if all item linked at boxes, else false
     */
    private boolean checkAllItemLinked(List<ItemGenetic> items) {
        boolean result = true;
        for(ItemGenetic item : items) {
            if (item.getBox() == null) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * Fill the box with items
     * @param box Box at fill
     * @param itemsSrcClone Items src
     */
    private void fillBox(BoxGenetic box, List<ItemGenetic> itemsSrcClone) {
        int errorBeforeAutomaticFilling = 0;
        Random random = new Random();
        while(box.getLeftCapacity() > 0 && errorBeforeAutomaticFilling < ParamAlgoGenetique.MAX_ACCEPT_BEFORE_AUTOMATIC_FILLING) {
            // choose item
            int randomIdxItem = random.nextInt(itemsSrcClone.size());
            ItemGenetic randomItem = itemsSrcClone.get(randomIdxItem);
            // create constraints booleans  representative the problem
            boolean boxLessThanTwoColors = (box.getColors().size() < 2);
            boolean itemNotLinkedAtAnBox = (randomItem.getBox() == null);
            boolean boxNotContainsColor = !(box.getColors().contains(randomItem.getColor()));
            boolean hasCapacity = (box.getLeftCapacity() - randomItem.getWeight()) >= 0;
            // if respect the constraints
            if(itemNotLinkedAtAnBox && boxLessThanTwoColors && boxNotContainsColor && hasCapacity){
                addItemInBox(box, randomItem);
                //set errorBeforeAutomaticFilling at 0
                errorBeforeAutomaticFilling = 0;
            }
            // increase errorBeforeAutomaticFilling
            if(!hasCapacity || !itemNotLinkedAtAnBox) {
                errorBeforeAutomaticFilling++;
            }
        }

        if(errorBeforeAutomaticFilling == ParamAlgoGenetique.MAX_ACCEPT_BEFORE_AUTOMATIC_FILLING) {
            ItemGenetic itemFound = searchItem(box, itemsSrcClone);
            if(itemFound != null) {
                addItemInBox(box, itemFound);
            }
        }
    }

    /**
     * Search an item that it possible at add in the box
     * @param box Box
     * @param items List<Item>
     * @return iteam found or null
     */
    private ItemGenetic searchItem(BoxGenetic box, List<ItemGenetic> items) {
        ItemGenetic iteamFound = null;
        for (ItemGenetic item : items) {
            boolean itemNotLinkedAtAnBox = (item.getBox() == null);
            boolean hasCapacity = (box.getLeftCapacity() - item.getWeight()) >= 0;

            if(itemNotLinkedAtAnBox && hasCapacity && (iteamFound == null || (iteamFound.getWeight() < item.getWeight()))) {
                boolean boxLessThanTwoColors = (box.getColors().size() < 2);
                boolean boxContainsColor = (box.getColors().contains(item.getColor()));
                if(boxLessThanTwoColors) {
                    iteamFound = item;
                } else if(boxContainsColor){
                    iteamFound = item;
                }

                if(iteamFound != null && (iteamFound.getWeight() == box.getLeftCapacity())) {
                    break;
                }
            }
        }
        return iteamFound;
    }

    /**
     * Add item in the box
     * @param box Box
     * @param item Item
     */
    private void addItemInBox(BoxGenetic box, ItemGenetic item) {
        item.setBox(box);
        box.addItem(item);
        box.decreaseLeftCapacity(item);
        boolean boxLessThanTwoColors = (box.getColors().size() < 2);
        if(boxLessThanTwoColors) {
            box.addColor(item);
        }
    }

    /**
     * Do natural selection of the genesis
     * @param solutions the list of solution potential
     */
    private List<Solution> selection(final List<Solution> solutions) {
        // we sort by decreasing loss rate
        Collections.sort(solutions);
        // we keep the best solutions
        List<Solution> bestSolutions = new ArrayList<>();
        bestSolutions.add(solutions.get(0));
        int bestLostSpace = bestSolutions.get(0).getLostSpace();
        for(int i=1; i < solutions.size(); i++) {
            Solution solution = solutions.get(i);
            if(solution.getLostSpace() == bestLostSpace) {
                bestSolutions.add(solution);
            } else {
                break;
            }
        }
        // create the new generation
        List<Solution> newGeneration = new ArrayList<>();
        int nbMaxCrossover = (int) Math.ceil((double) ParamAlgoGenetique.SOLUTIONS_AT_GENERATE / 4);
        for(int i = 0 ; i < nbMaxCrossover; i++) {
            if(ParamAlgoGenetique.CROSSOVER_TYPE.equals(CrossoverType.BASIC)) {
                newGeneration.addAll(crossover4Childs(solutions.get(i),solutions.get(i+1)));
            } else if(ParamAlgoGenetique.CROSSOVER_TYPE.equals(CrossoverType.BEST_SOLUTION)) {
                newGeneration.addAll(crossover4Childs(solutions.get(0),solutions.get(i+1)));
            } else if(ParamAlgoGenetique.CROSSOVER_TYPE.equals(CrossoverType.BEST_WITH_BAD)) {
                newGeneration.addAll(crossover4Childs(solutions.get(i),solutions.get(solutions.size()-(1+i))));
            }
        }
        // remove extra childs
        if(newGeneration.size() > ParamAlgoGenetique.SOLUTIONS_AT_GENERATE) {
            newGeneration.subList(ParamAlgoGenetique.SOLUTIONS_AT_GENERATE, newGeneration.size()).clear();
        }
        // remove some number childs for add the best solutions in the new generation
        newGeneration.subList(newGeneration.size()-bestSolutions.size(), newGeneration.size()).clear();
        newGeneration.addAll(bestSolutions);
        return newGeneration;
    }

    /**
     * Create 4 children from mother and father solution
     * @param father father solution
     * @param mother mother solution
     * @return a list of children created
     */
    private List<Solution> crossover4Childs(final Solution father, final Solution mother) {
        List<Solution> childs = new ArrayList<>();
        int boxTakenFromFather = roundDouble( (double) father.getBoxes().size() /2 );
        int boxTakenFromMother = roundDouble( (double) mother.getBoxes().size() /2 );

        for(int i=1; i <= 4; i++) {
            Solution child = new Solution();
            switch (i) {
                case 1:
                    child.getBoxes().addAll(copyBoxes(father.getBoxes().subList(0, boxTakenFromFather)));
                    child.getBoxes().addAll(copyBoxes(mother.getBoxes().subList(0, boxTakenFromMother)));
                    break;
                case 2:
                    child.getBoxes().addAll(copyBoxes(father.getBoxes().subList(boxTakenFromFather, father.getBoxes().size())));
                    child.getBoxes().addAll(copyBoxes(mother.getBoxes().subList(boxTakenFromMother, mother.getBoxes().size())));
                    break;
                case 3:
                    child.getBoxes().addAll(copyBoxes(father.getBoxes().subList(boxTakenFromFather, father.getBoxes().size())));
                    child.getBoxes().addAll(copyBoxes(mother.getBoxes().subList(0, boxTakenFromMother)));
                    break;
                case 4:
                    child.getBoxes().addAll(copyBoxes(father.getBoxes().subList(0, boxTakenFromFather)));
                    child.getBoxes().addAll(copyBoxes(mother.getBoxes().subList(boxTakenFromMother, mother.getBoxes().size())));
                    break;
            }
            child.setCapacityMax(Math.max(father.getCapacityMax(), mother.getCapacityMax()));
            childs.add(child);
        }
        return childs;
    }

    /**
     * round the number, if the number is 16.4, the result is 16. If the number 17.6, the result is 18.
     * @param number the number to round
     * @return the rounded number
     */
    private int roundDouble(double number){
        double decimal = number - (int) number;
        if(decimal < 0.5) {
            number = Math.floor(number);
        } else {
            number = Math.ceil(number);
        }
        return (int) number;
    }

    /**
     * Copy all content of list and return the new list of boxes with new items copied
     * @param boxesAtCopy the list of boxes at copy
     * @return the new list of box copied
     */
    private List<BoxGenetic> copyBoxes(List<BoxGenetic> boxesAtCopy) {
        List<BoxGenetic> newBoxes = new ArrayList<>();
        for(BoxGenetic boxAtCopy : boxesAtCopy) {
            BoxGenetic newBox = new BoxGenetic(boxAtCopy);
            newBox.setColors(newBox.getColors());
            for(Map.Entry<Integer, ItemGenetic> entry : boxAtCopy.getItems().entrySet()) {
                ItemGenetic newItem = new ItemGenetic(entry.getValue());
                newItem.setBox(newBox);
                newBox.addItem(newItem);
            }
            newBoxes.add(newBox);
        }
        return newBoxes;
    }

    /**
     * Mutation the solution
     * @param solutions Solution
     * @param itemsSrc list of items src
     * @param boxesSrc list of boxes src
     */
    private void mutation(final List<Solution> solutions, final List<ItemGenetic> itemsSrc, final List<BoxGenetic> boxesSrc ) {

        for(Solution solution : solutions) {

            List<Integer> idItemPresents = new ArrayList<>();
            List<ItemGenetic> itemDuplicates = new ArrayList<>();

            fillIdItemPresentsAndItemDuplicates(solution, idItemPresents, itemDuplicates);
            deleteItemDuplicates(solution, itemDuplicates);
            refreshSolution(solution);

            List<Integer> idItemLosts = getListIdItemLosts(itemsSrc, idItemPresents);

            int nbError = 1;
            while (!idItemLosts.isEmpty()) {
                // sort boxes for win lost space
                sortBoxesToGainLostSpace(solution);
                // add item lost in the box
                addItemLostInTheSolution(itemsSrc, solution, idItemPresents, idItemLosts);

                if(!idItemLosts.isEmpty() && (nbError == ParamAlgoGenetique.MAX_ACCEPT_BEFORE_AUTOMATIC_SEARCH_BOX)) {
                    int idItemLost = idItemLosts.get(0);
                    ItemGenetic itemLost = new ItemGenetic(itemsSrc.get(idItemLost));
                    int i = 0;
                    // search a box to put the article
                    while (itemLost.getBox() == null) {
                        BoxGenetic box = boxesSrc.get(i);
                        if(box.getCapacity() >= itemLost.getWeight()) {
                            BoxGenetic boxAtAdd = new BoxGenetic(box);
                            boxAtAdd.addItem(itemLost);
                            boxAtAdd.addColor(itemLost);
                            boxAtAdd.decreaseLeftCapacity(itemLost);
                            itemLost.setBox(boxAtAdd);
                            solution.addBox(boxAtAdd);
                        }
                        i++;
                    }
                    idItemPresents.add(itemLost.getId());
                    idItemLosts.removeIf(idItemPresents::contains);
                    nbError = 1;
                } else {
                    nbError++;
                }
            }
            removeBoxEmpty(solution);
            refreshSolution(solution);
        }
        refreshSolutions(solutions);
    }

    /**
     * Sort the boxes of solution for win lost space
     * @param solution Solution
     */
    private void sortBoxesToGainLostSpace(Solution solution) {
        for (BoxGenetic boxForStoring : solution.getBoxes()) {
            if (boxForStoring.getLeftCapacity() == 0) {
                continue;
            }
            for (BoxGenetic box : solution.getBoxes()) {
                if (boxForStoring.equalsBox(box)) {
                    continue;
                }
                List<Integer> listIdItemsStored = new ArrayList<>();
                List<ItemGenetic> items = new ArrayList<>(box.getItems().values());
                items.sort(Comparator.comparing(ItemGenetic::getWeight));

                for (ItemGenetic item : items) {
                    boolean isItemNotEnter = item.getWeight() > boxForStoring.getLeftCapacity();
                    if (isItemNotEnter) {
                        break;
                    }
                    ItemGenetic itemHasStore = new ItemGenetic(item);
                    tryAddItemInBox(boxForStoring, listIdItemsStored, itemHasStore);
                }
                box.getItems().entrySet().removeIf(entry -> listIdItemsStored.contains(entry.getKey()));
                if (box.getItems().isEmpty()) {
                    box.getColors().clear();
                }
                List<Integer> colorsBox = new ArrayList<>();
                int capacityTake = 0;
                for (Map.Entry<Integer, ItemGenetic> entry : box.getItems().entrySet()) {
                    if (!colorsBox.contains(entry.getValue().getColor())) {
                        colorsBox.add(entry.getValue().getColor());
                    }
                    capacityTake += entry.getValue().getWeight();
                }
                box.setColors(colorsBox);
                box.setLeftCapacity(box.getCapacity() - capacityTake);
            }
        }
    }

    /**
     * try to add items to the box by controlling the color constraint
     * @param boxForStoring box for storing
     * @param listIdItems list id items
     * @param newItem item
     */
    private void tryAddItemInBox(BoxGenetic boxForStoring, List<Integer> listIdItems, ItemGenetic newItem) {
        if (boxForStoring.getColors().size() < 2) {
            addItemInBox(boxForStoring, newItem);
            listIdItems.add(newItem.getId());
        } else if (boxForStoring.getColors().size() == 2 && boxForStoring.getColors().contains(newItem.getColor())) {
            addItemInBox(boxForStoring, newItem);
            listIdItems.add(newItem.getId());
        }
    }

    /**
     * remove box empty in the solution
     * @param solution the solution
     */
    private void removeBoxEmpty(Solution solution) {
        List<BoxGenetic> boxEmpty = new ArrayList<>();
        for(BoxGenetic box : solution.getBoxes()) {
            if(box.getItems().size() == 0) {
                boxEmpty.add(box);
            }
        }
        if(!boxEmpty.isEmpty()) {
            solution.getBoxes().removeAll(boxEmpty);
        }
    }

    /**
     * add item lost in the solution
     * @param itemsSrc list of item src
     * @param solution solution
     * @param idItemPresents list of item presents
     * @param idItemLosts list of item losts
     */
    private void addItemLostInTheSolution(List<ItemGenetic> itemsSrc, Solution solution, List<Integer> idItemPresents, List<Integer> idItemLosts) {
        for(Integer id : idItemLosts) {
            for(BoxGenetic box : solution.getBoxes()) {
                ItemGenetic addItemLost = new ItemGenetic(itemsSrc.get(id));
                if(addItemLost.getWeight() <= box.getLeftCapacity()) {
                    tryAddItemInBox(box, idItemPresents, addItemLost);
                }
                if(addItemLost.getBox() != null) {
                    break;
                }
            }
        }
        idItemLosts.removeIf(idItemPresents::contains);
    }

    /**
     * Get list of id item losts in the solution
     * @param itemsSrc list itemSrc
     * @param idItemPresents list item presents in the solution
     * @return List<Integer> id items losts
     */
    private List<Integer> getListIdItemLosts(List<ItemGenetic> itemsSrc, List<Integer> idItemPresents) {
        List<Integer> idItemLosts = new ArrayList<>();
        for(ItemGenetic item : itemsSrc) {
            if(!idItemPresents.contains(item.getId())) {
                idItemLosts.add(item.getId());
            }
        }
        return idItemLosts;
    }

    /**
     * Remove items duplicates in the solution
     * @param solution solution
     * @param itemDuplicates list of items duplicates
     */
    private void deleteItemDuplicates(Solution solution, List<ItemGenetic> itemDuplicates) {
        for(ItemGenetic itemDuplicate : itemDuplicates) {
            for(BoxGenetic box : solution.getBoxes()) {
                box.getItems().remove(itemDuplicate.getId(), itemDuplicate);
            }
        }
    }

    /**
     * fill id item presents and items duplicates
     * @param solution solution
     * @param idItemPresents list of integer
     * @param itemDuplicates list of item
     */
    private void fillIdItemPresentsAndItemDuplicates(Solution solution, List<Integer> idItemPresents, List<ItemGenetic> itemDuplicates) {
        for(BoxGenetic box : solution.getBoxes()) {
            for (Map.Entry<Integer, ItemGenetic> entry : box.getItems().entrySet()) {
                if (!idItemPresents.contains(entry.getKey())) {
                    idItemPresents.add(entry.getKey());
                } else {
                    itemDuplicates.add(entry.getValue());
                }
            }
        }
    }

    /**
     * Refresh a solution (recalcule the number colors of the box, the left capacity of the box, link the item at the box, the lost space and capacity max of solution)
     * @param solutions the list of solution
     */
    private void refreshSolutions(final List<Solution> solutions) {
        for(Solution solution : solutions) {
            refreshSolution(solution);
        }
        Collections.sort(solutions);
    }

    /**
     * Refresh a solution (recalcule the number colors of the box, the left capacity of the box, link the item at the box, the lost space and capacity max of solution)
     * @param solution solution at refresh
     */
    private void refreshSolution(Solution solution) {
        int capacityMax = 0;
        int lostSpace = 0;
        solution.setLostSpace(0);
        solution.setCapacityMax(0);
        for(BoxGenetic box : solution.getBoxes()) {
            int leftCapacity = 0;
            List<Integer> colors = new ArrayList<>();
            box.setLeftCapacity(box.getCapacity());
            for(Map.Entry<Integer, ItemGenetic> entry : box.getItems().entrySet() ) {
                leftCapacity += entry.getValue().getWeight();
                capacityMax += entry.getValue().getWeight();
                if(!colors.contains(entry.getValue().getColor())) {
                    colors.add(entry.getValue().getColor());
                }
                entry.getValue().setBox(box);
            }
            box.setLeftCapacity(box.getCapacity()-leftCapacity);
            box.setColors(colors);
            lostSpace += box.getLeftCapacity();
        }
        solution.setCapacityMax(capacityMax);
        solution.setLostSpace(lostSpace);
    }
}
