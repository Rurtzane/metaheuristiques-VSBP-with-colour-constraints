package algoGenetique;

public class ItemGenetic {
	private Integer id;
	private Integer weight;
	private Integer color;
	private BoxGenetic box;

	public ItemGenetic(ItemGenetic item) {
		this.id = item.getId();
		this.weight = item.getWeight();
		this.color = item.getColor();
		this.box = null;
	}

	public ItemGenetic() {
	}

	@Override
	public String toString() {
		return "Id : " + id + "\nWeight : " + weight + "\nColor : " + color + "\n";
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public BoxGenetic getBox() {
		return box;
	}

	public void setBox(BoxGenetic box) {
		this.box = box;
	}
}
