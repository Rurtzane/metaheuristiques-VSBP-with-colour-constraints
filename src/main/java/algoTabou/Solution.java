package algoTabou;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

/**
 * Solution pour la recherche tabou
 */
public class Solution implements Cloneable {
	private List<Item> items;
	private List<Box> boxes;
	private int espacePerdu;
	private List<Integer> capacities;
	
	private int nbIter;
	private long time;

	public Solution(List<Item> items, List<Box> boxes, List<Integer> capacities) {
		this.items = items;
		this.boxes = boxes;
		boxes.forEach(box -> espacePerdu += box.getLeftCapacity());
		this.capacities = capacities;
	}
	
	public Solution(String path) throws CloneNotSupportedException {
		long time = System.currentTimeMillis();
		items = new ArrayList<Item>();
		boxes = new ArrayList<Box>();
		capacities = new ArrayList<Integer>();
		
		init(path);
		
		this.time = System.currentTimeMillis() - time;
	}

	public List<Box> getBoxes() {
		return boxes;
	}

	public void setBoxes(List<Box> boxes) {
		this.boxes = boxes;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public int getEspacePerdu() {
		return espacePerdu;
	}

	public void setEspacePerdu(int espacePerdu) {
		this.espacePerdu = espacePerdu;
	}
	
	public int getNbIter() {
		return nbIter;
	}

	public void setNbIter(int nbIter) {
		this.nbIter = nbIter;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * Calcul et retourne une solution initiale
	 * 
	 * @param path	nom du bench
	 * 
	 * @return	Une solution initiale
	 */
	public void init(String path) {
		try {
			FileReader.readData(items, boxes, path);
			
			items.sort(new Comparator<Item>() {

				@Override
				public int compare(Item o1, Item o2) {
					if (o1.getId() < o2.getId()) {
						return -1;
					}

					if (o1.getId() > o2.getId()) {
						return 1;
					}

					return 0;
				}

			});

			//Récupération des capacités des boites
			boxes.forEach(box -> {
				capacities.add(box.getCapacity());
			});
			boxes.clear();
			capacities.sort(Comparator.naturalOrder());
			
			for (Item item : items) {
				for (Integer capacity : capacities) {
					if (capacity >= item.getWeight()) {
						Box newBox = new Box(capacity);
						newBox.add(item);
						boxes.add(newBox);
						break;
					}
				}
			}
			
			//Calcul de l'espace perdu
			calculEspaceRestant();
			
//			System.out.println("Liste des capacités : " + capacities);
//			System.out.println("Liste des " + boxes.size() + " boîtes : " + boxes);
//			System.out.println("Espace restant : " + espacePerdu);
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Création de voisinage en essayant de diminuer le nombre de boites
	 * 
	 * @return Voisinage de la solution
	 * 
	 * @throws CloneNotSupportedException
	 */
	public List<Solution> downBoxes() throws CloneNotSupportedException {
		List<Solution> neighbours = new ArrayList<Solution>();
		Solution solInter;
		Solution solFin;
		boolean itemAdded = false;
		
		for (int i = 0; i < boxes.size(); i++) {
			Box box = boxes.get(i);
			
			//Si la boîte peut être agrandie
			if (box.getCapacity() < capacities.get(capacities.size() - 1)) {
				//Agrandissement de la boîte à la première capacité supérieure trouvée
				for (Integer capacity : capacities) {
					if (capacity > box.getCapacity()) {
						solInter = (Solution) this.clone();
						Box boxInter = solInter.getBoxes().get(i);
						boxInter.setCapacity(capacity);
						
						for (Item item : solInter.getItems()) {
							solFin = (Solution) solInter.clone();
							Box boxFin = solFin.getBoxes().get(i);
							if (!boxFin.getItems().containsKey(item.getId()) && item.getWeight() <= boxFin.getLeftCapacity()) {
								Box oldBox = solFin.getItems().get(item.getId()).getBox();	//item.getBox();
								if (boxFin.canAdd(item)) {
									oldBox.remove(item);
									boxFin.add(item);
									itemAdded = true;
									if (oldBox.getLeftCapacity() == oldBox.getCapacity()) {
										solFin.getBoxes().remove(oldBox);
									} else {
										for (Integer newCapacity : capacities) {
											if (newCapacity >= oldBox.getCapacity() - oldBox.getLeftCapacity()) {
												oldBox.setCapacity(newCapacity);
												break;
											}
										}
									}
									
									solFin.calculEspaceRestant();
									neighbours.add(solFin);
								}
							}
						}
						
						if (!itemAdded) {
							solInter.calculEspaceRestant();
							neighbours.add(solInter);
						}
						
						itemAdded = false;
						
						break;
					}
				}
				
			}
		}
		
		return neighbours;
	}
	
	/**
	 * Création de voisinage en essayant d'augmenter le nombre de boites
	 * 
	 * @return Voisinage de la solution
	 * 
	 * @throws CloneNotSupportedException
	 */
	public List<Solution> upBoxes() throws CloneNotSupportedException {
		List<Solution> neighbours = new ArrayList<Solution>();
		Solution solInter;
			
			for (Integer capacity : capacities) {
				solInter = (Solution) this.clone();
				for (Item item : solInter.getItems()) {
					if (capacity >= item.getWeight()) {
						Box oldBox = item.getBox();
						oldBox.remove(item);
						if (oldBox.getLeftCapacity() == oldBox.getCapacity()) {
							solInter.getBoxes().remove(oldBox);
						}
						Box newBox = new Box(capacity);
						newBox.add(item);
							
						solInter.getBoxes().add(newBox);
						solInter.calculEspaceRestant();
						neighbours.add(solInter);
					}
				}
			}

		return neighbours;
	}
	
	public List<Solution> permutation() throws CloneNotSupportedException {
		List<Solution> neighbours = new ArrayList<Solution>();
		Solution sol = (Solution) this.clone();

		for (int i = 0; i < sol.getItems().size(); i++) {
			Item currentItem = sol.getItems().get(i);
			Box oldBox = currentItem.getBox();
			for (int j = 0; j < sol.getBoxes().size(); j++) {
				Box currentBox = sol.getBoxes().get(j);
				if (!currentBox.getItems().containsKey(currentItem.getId())) {
					if (currentBox.canAdd(currentItem)) {
						oldBox.remove(currentItem);
						currentBox.add(currentItem);
						sol.calculEspaceRestant();
						neighbours.add(sol);
						sol = (Solution) this.clone();
					}
				}
			}
		}

		return neighbours;
	}
	
	/**
	 * Création du voisinage de la solution
	 * 
	 * @return	Le voisinage de la solution
	 * 
	 * @throws CloneNotSupportedException
	 */
	public List<Solution> creerVoisinage() throws CloneNotSupportedException {
		List<Solution> neighbours = new ArrayList<Solution>();
		neighbours.addAll(downBoxes());
		neighbours.addAll(upBoxes());
//		neighbours.addAll(permutation());
		
		//Tri des solutions par espace restant croissant
		neighbours.sort(new Comparator<Solution>() {
			
			@Override
			public int compare(Solution o1, Solution o2) {
				if (o1.getEspacePerdu() < o2.getEspacePerdu()) {
					return -1;
				}
				
				if (o1.getEspacePerdu() > o2.getEspacePerdu()) {
					return 1;
				}
				
				return 0;
			}
			
		});
		
		return neighbours;
	}
	
	/**
	 * Calcul de l'espace restant de la solution
	 */
	private void calculEspaceRestant() {
		espacePerdu = 0;
		
		boxes.forEach(box -> {
			espacePerdu += box.getLeftCapacity();	
		});
	}
	
	private void reduireBoite(Box box) {
		for (Integer capacity : capacities) {
			if (capacity >= (box.getCapacity() - box.getLeftCapacity())) {
				box.setCapacity(capacity);
				break;
			}
		}
	}

	@Override
	public String toString() {
		return "Solution : \nEspace restant : " + espacePerdu + "\n" + boxes + "\n\n";
	}

	@Override
	public boolean equals(Object obj) {
		return boxes.equals(((Solution) obj).getBoxes());
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		List<Item> newItems = new ArrayList<Item>();
		List<Box> newBoxes = new ArrayList<Box>();
		for (Box box : boxes) {
			Box newBox = new Box(box.getCapacity());
			for (Entry<Integer, Item> item : box.getItems().entrySet()) {
				Item newItem = new Item(item.getValue());
				newItems.add(newItem);
				newBox.add(newItem);
			}
			newBoxes.add(newBox);
		}
		
		newItems.sort(new Comparator<Item>() {
			
			@Override
			public int compare(Item o1, Item o2) {
				if (o1.getId() < o2.getId()) {
					return -1;
				}
				
				if (o1.getId() > o2.getId()) {
					return 1;
				}
				
				return 0;
			}
			
		});
		
		Solution clone = new Solution(newItems, newBoxes, capacities);
		
		return clone;
	}
}
