package algoTabou;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class FileReader {

	public static void readData(List<Item> items, List<Box> boxes, String fileName) throws FileNotFoundException {
		File file = new File("benchs/"+fileName);

		Scanner s = new Scanner(file);

		int value = Integer.parseInt(s.next());

//		System.out.println("NB_SAC : " + value);
		for (int i = 0; i < value; i++) {
			Box box = new Box();
			int capacity = Integer.parseInt(s.next());
			box.setCapacity(capacity);
			box.setLeftCapacity(capacity);
			boxes.add(box);
		}
//		System.out.println(/*"NB_COULEUR : " + */Integer.parseInt(s.next()));
//		System.out.println(/*"NB_OBJET : " + */Integer.parseInt(s.next()));
		Integer.parseInt(s.next());
		Integer.parseInt(s.next());

		int id = 0;
		while (s.hasNext()) {
			Item item = new Item();
			item.setWeight(Integer.parseInt(s.next()));
			item.setColor(Integer.parseInt(s.next()));
			item.setId(id);
			items.add(item);
			id++;
		}
		s.close();
	}
}
