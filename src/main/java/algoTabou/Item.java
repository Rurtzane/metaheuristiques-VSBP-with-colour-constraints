package algoTabou;

public class Item {
	private Integer id;
	private Integer weight;
	private Integer color;
	private Box box;
	
	public Item(int id, int weight, int color) {
		this.id = id;
		this.weight = weight;
		this.color = color;
	}
	
	public Item(Item item) {
		this.id = item.getId();
		this.weight = item.getWeight();
		this.color = item.getColor();
		this.box = null;
	}
	
	public Item() {
	}

	@Override
	public String toString() {
		return "Id : " + id + "\nWeight : " + weight + "\nColor : " + color + "\n";
	}
//
//    @Override
//    public boolean equals(Object obj) {
//        return id == ((Item) obj).getId();
//    }
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public Box getBox() {
		return box;
	}

	public void setBox(Box box) {
		this.box = box;
	}
}
