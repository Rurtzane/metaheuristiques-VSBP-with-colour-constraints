package algoTabou;

import java.util.*;
import java.util.Map.Entry;

public class Box {
	private final int MAX_COLORS = 2;
	
	private int capacity;
	private int leftCapacity;
	private List<Integer> colors;
	private Map<Integer, Item> items;
	
	public Box(int capacity) {
		this.capacity = capacity;
		leftCapacity = capacity;
		items = new HashMap<Integer, Item>();
		colors = new ArrayList<Integer>(MAX_COLORS);
		for (int i = 0; i < MAX_COLORS; i++) {
			colors.add(-1);
		}
	}

	public Box() {
		colors = new ArrayList<>();
		items = new HashMap<>();
	}

    public Box(Box box) {
        this.capacity = box.getCapacity();
        this.leftCapacity = box.getLeftCapacity();
		this.colors = new ArrayList<>();
		this.items = new HashMap<>();
    }

	public boolean canAdd(Item item) {
		if (leftCapacity >= item.getWeight()) {
			for (int i = 0; i < MAX_COLORS; i++) {
				if (colors.get(i) == -1 || colors.get(i) == item.getColor()) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Add an item to the box
	 * 
	 * @param item	Item to add
	 * @return	True if the item has been added, false otherwise
	 */
	public boolean add(Item item) {
		if (leftCapacity >= item.getWeight()) {
			for (int i = 0; i < MAX_COLORS; i++) {
				if (colors.get(i) == -1 || colors.get(i) == item.getColor()) {
					items.put(item.getId(), item);
					item.setBox(this);
					leftCapacity -= item.getWeight();
					if (colors.get(i) == -1) {
						colors.set(i, item.getColor());
					}
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean remove(Item item) {
		if (items.remove(item.getId()) != null) {
			leftCapacity += item.getWeight();
			colors.clear();
			for (Entry<Integer, Item> it : items.entrySet()) {
				colors.add(it.getValue().getColor());
			}
			for (int i = colors.size(); i < MAX_COLORS; i++) {
				colors.add(-1);
			}
			item.setBox(null);
			return true;
		}
		
		return false;
	}

	/**
	 * Add an item to the box
	 * @param item item
	 */
	public void addItem(Item item) {
		items.put(item.getId(), item);
	}

	/**
	 * Decrease the left capacity of the box with the capcity of item
	 * @param item Item
	 */
	public void decreaseLeftCapacity(Item item) {
		this.leftCapacity -= item.getWeight();
	}

	/**
	 * Add the color to the box
	 * @param item Item
	 */
	public void addColor(Item item) {
		colors.add(item.getColor());
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getLeftCapacity() {
		return leftCapacity;
	}
	
	public void setLeftCapacity(int leftCapacity) {
		this.leftCapacity = leftCapacity;
	}
	
	public List<Integer> getColors() {
		return colors;
	}
	
	public void setColors(List<Integer> colors) {
		this.colors = colors;
	}
	
	public Map<Integer, Item> getItems() {
		return items;
	}

	public void setItems(Map<Integer, Item> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Total capacity : " + capacity + "\nLeft capacity : " + leftCapacity 
				+ "\nNumber of items : " + items.size() +  "\nItems : \n" + items + "\n\n";
	}

    @Override
    public boolean equals(Object obj) {
        Box box = (Box) obj;

        return capacity == box.getCapacity() && items.equals(box.getItems());
    }

    public boolean equalsBox(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Box box = (Box) o;
        return  capacity == box.capacity &&
				leftCapacity == box.leftCapacity &&
                Objects.equals(colors, box.colors) &&
                Objects.equals(items, box.items);
    }
}
