package algoTabou;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.opencsv.CSVWriter;

/**
 * Export des résultats de la recherche taboue basé sur opencsv
 */
public class Export {
	public static void export(String path, List<Solution> sol, int tabu, int maxIter, int nbEloignement) {
		File file = new File(path);
		
		try {
			FileWriter out = new FileWriter(file);
			CSVWriter writer = new CSVWriter(out);
			
			//Paramètres
			String[] global = {"Tabu", String.valueOf(tabu), "Max iter", String.valueOf(maxIter), "Nb Eloignement", String.valueOf(nbEloignement)};
			writer.writeNext(global);
			
			String[] solutions = new String[sol.size() + 3];
			solutions[0] = "Solution";
			solutions[1] = "Init";
			
			//Affichage du nombre de solutions
			for (int i = 1; i < sol.size(); i++) {
				solutions[i + 1] = String.valueOf(i);
			}
			solutions[sol.size() + 1] = "Avg";
			writer.writeNext(solutions);
			
			String[] values = new String[sol.size() + 2];
			String[] time = new String[sol.size() + 2];
			String[] iter = new String[sol.size() + 2];
			values[0] = "Values";
			time[0] = "Time (ms)";
			iter[0] = "Iter";
			float avgValue = 0, avgTime = 0, avgIter = 0;
			
			//Affichage des résultats (espace restant, temps et nombre d'itération)
			for (int i = 1; i <= sol.size(); i++) {
				values[i] = String.valueOf(sol.get(i - 1).getEspacePerdu());
				time[i] = String.valueOf(sol.get(i - 1).getTime());
				iter[i] = String.valueOf(sol.get(i - 1).getNbIter());
				avgValue += Float.parseFloat(values[i]);
				avgTime += Float.parseFloat(time[i]);
				avgIter += Float.parseFloat(iter[i]);
			}
			
			//Retrait de la solution initiale aux moyennes
			avgValue -= Float.parseFloat(values[1]);
			avgTime -= Float.parseFloat(time[1]);
			avgIter -= Float.parseFloat(iter[1]);
			
			//Calcul des moyennes
			avgValue /= (sol.size() - 1);
			avgTime /= (sol.size() - 1);
			avgIter /= (sol.size() - 1);
			values[sol.size() + 1] = String.valueOf(avgValue);
			time[sol.size() + 1] = String.valueOf(avgTime);
			iter[sol.size() + 1] = String.valueOf(avgIter);
			
			writer.writeNext(values);
			writer.writeNext(time);
			writer.writeNext(iter);
			
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
