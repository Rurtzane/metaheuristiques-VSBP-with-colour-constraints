package algoTabou;

import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) throws CloneNotSupportedException {
		System.out.println("DEBUT");
		long time = System.currentTimeMillis();
		List<Solution> solutions = new ArrayList<Solution>();
		final int tabu = 10000;
		final int maxIter = 1;
		final int nbEloignement = 100;
		final String bench = "bench_10_16";
		
		//appel pour test solInitial
		Solution sol = new Solution(bench);
//		System.out.println(sol.upBoxes());
		solutions.add(sol);
		System.out.println("Init : " + sol.getEspacePerdu());
		System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
		for (int i = 0; i < 1; i++) {
			time = System.currentTimeMillis();
			Solution solution = Tabou.recherche(tabu, maxIter, nbEloignement, bench);
			solutions.add(solution);
			System.out.println("RESULTAT " + (i+1) + " : " + solution);
			System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
		}
//		Export.export("test5", solutions, tabu, maxIter, nbEloignement);
//		solutions.clear();
//		solutions.add(sol);
//		for (int i = 0; i < 10; i++) {
//			time = System.currentTimeMillis();
//			Solution solution = Tabou.recherche(300, maxIter, maxNeighSearch, bench);
//			solutions.add(solution);
//			System.out.println("RESULTAT " + (i+1) + " : " + solution.getEspacePerdu());
//			System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
//		}
//		Export.export("test2", solutions, 300, maxIter, maxNeighSearch);
//		solutions.clear();
//		solutions.add(sol);
//		for (int i = 0; i < 10; i++) {
//			time = System.currentTimeMillis();
//			Solution solution = Tabou.recherche(500, maxIter, maxNeighSearch, bench);
//			solutions.add(solution);
//			System.out.println("RESULTAT " + (i+1) + " : " + solution.getEspacePerdu());
//			System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
//		}
//		Export.export("test3", solutions, 500, maxIter, maxNeighSearch);
//		solutions.clear();
//		solutions.add(sol);
//		for (int i = 0; i < 10; i++) {
//			time = System.currentTimeMillis();
//			Solution solution = Tabou.recherche(750, maxIter, maxNeighSearch, bench);
//			solutions.add(solution);
//			System.out.println("RESULTAT " + (i+1) + " : " + solution.getEspacePerdu());
//			System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
//		}
//		Export.export("test4", solutions, 750, maxIter, maxNeighSearch);
////		solutions.clear();
////		solutions.add(sol);
////		for (int i = 0; i < 10; i++) {
////			time = System.currentTimeMillis();
////			Solution solution = Tabou.recherche(10000, maxIter, maxNeighSearch, bench);
////			solutions.add(solution);
////			System.out.println("RESULTAT " + (i+1) + " : " + solution.getEspacePerdu());
////			System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
////		}
////		Export.export("test5", solutions, 10000, maxIter, maxNeighSearch);
////		solutions.clear();
////		solutions.add(sol);
////		Export.export("test10", solutions, 1000, maxIter, maxNeighSearch);
////		solutions.clear();
////		solutions.add(sol);
////		for (int i = 0; i < 10; i++) {
////			time = System.currentTimeMillis();
////			Solution solution = Tabou.recherche(3000, maxIter, maxNeighSearch, bench);
////			solutions.add(solution);
////			System.out.println("RESULTAT " + (i+1) + " : " + solution.getEspacePerdu());
////			System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
////		}
////		Export.export("test11", solutions, 3000, maxIter, maxNeighSearch);
////		solutions.clear();
////		solutions.add(sol);
////		for (int i = 0; i < 10; i++) {
////			time = System.currentTimeMillis();
////			Solution solution = Tabou.recherche(5000, maxIter, maxNeighSearch, bench);
////			solutions.add(solution);
////			System.out.println("RESULTAT " + (i+1) + " : " + solution.getEspacePerdu());
////			System.out.println("FIN (" + (System.currentTimeMillis() - time) + " milisecondes)");
////		}
////		Export.export("test12", solutions, 5000, maxIter, maxNeighSearch);
////		Export.export(bench, solutions, tabu, maxIter, maxNeighSearch);
	}

}
