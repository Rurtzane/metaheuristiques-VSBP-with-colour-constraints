package algoTabou;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Recherche tabou
 */

public abstract class Tabou {
	/**Liste taboue*/
	private static Queue<Solution> tabou;
	
	/**Meilleure solution trouvée*/
	private static Solution bestSolution;

	/**
	 * Algorithme de recherche tabou
	 * 
	 * @param tailleTabou	Taille de la liste tabou
	 * @param iterMax		Nombre d'itération maximum sans amélioration (une itération = phase d'intensification + phase de diversification)
	 * @param nbEloignement	Nombre de solutions parcourues pendant la phase de diversification
	 * @param path			Nom du bench
	 * 
	 * @return	La meilleure solution toruvée
	 * 
	 * @throws CloneNotSupportedException
	 */
	public static Solution recherche(final int tailleTabou, final int iterMax, final int nbEloignement, final String path) throws CloneNotSupportedException {
		long time = System.currentTimeMillis();
		tabou = new ArrayDeque<Solution>(tailleTabou);

		//Création de la solution initiale
		Solution solInit = new Solution(path);
		
		//Création de la solution courante
		Solution solActu = solInit;
		
		//Initialisation de la meilleure solution globale
		bestSolution = solInit;
		
		//Création de la meilleure solution locale
		Solution bestSol = solInit;
		
		//Création du voisinage et stockage du meilleur voisinage
		List<Solution> voisinsBestSol = solActu.creerVoisinage();
		List<Solution> voisinageCourant = voisinsBestSol;
		
		int totalIter = 0;
		int iter = 0;
		int cpt = 0;
		boolean newBest = false;
		boolean diversification = false;

		while (iter < iterMax && bestSol.getEspacePerdu() > 0) {
			newBest = false;
			
			//Recherche d'une meilleure solution dans le voisinage direct de la solution courante
			Solution tmp = rechercheMeilleurSolutionDansVoisinage(voisinageCourant, bestSol);
			
			//Si nouvelle meilleure solution locale trouvée, alors mise à jour des informations
			if (tmp.getEspacePerdu() < bestSol.getEspacePerdu()) {
				diversification = false;
				bestSol = tmp;
				if (bestSol.getEspacePerdu() < bestSolution.getEspacePerdu()) {
					bestSolution = bestSol;
					iter = 0;
				}
				addTabou(bestSol, tailleTabou);
				solActu = bestSol;
				voisinsBestSol = solActu.creerVoisinage();
				voisinageCourant = voisinsBestSol;
//				System.out.println("New Best : " + bestSol.getEspacePerdu() + " at " + (System.currentTimeMillis() - time) + " for " + totalIter + " iter");
				newBest = true;
			}

			/*
			 * Si pas de meilleure solution trouvée et en phase d'intensification,
			 * alors recherche de voisinage du voisinage de la solution courante
			 */
//			if (!newBest && !diversification) {
//				int j = 0;
//				while (j < voisinageCourant.size()) {
//					System.out.println("Compteur : " + j + " / " + voisinageCourant.size());
//					Solution tmp2 = rechercheMeilleurSolutionDansVoisinage(voisinageCourant.get(j).creerVoisinage(), bestSol);
//					if (tmp2.getEspacePerdu() < bestSol.getEspacePerdu()) {
//						bestSol = tmp2;
//						if (bestSol.getEspacePerdu() < bestSolution.getEspacePerdu()) {
//							bestSolution = bestSol;
//							iter = 0;
//						}
//						addTabou(bestSol, tailleTabou);
//						solActu = bestSol;
//						voisinsBestSol = solActu.creerVoisinage();
//						voisinageCourant = voisinsBestSol;
//						System.out.println("New Best 2 : " + bestSol.getEspacePerdu() + " at " + (System.currentTimeMillis() - time) + " for " + totalIter + " iter");
//						newBest = true;
//						break;
//					}
//					j++;
//				}
//			}
			
			//Si toujours pas de nouvelle solution, alors phase de diversification
			if (!newBest) {
				diversification = true;
			}

			/*
			 * Phase de diversification : 
			 * éloignement de la solution courante de manière aléatoire 
			 * en repartant de la meilleure solution trouvée (solution globale)
			 */
			if (diversification) {
				int random;

				//Récupération du nombre de voisins de la meilleure solution
				List<Integer> index = new ArrayList<Integer>();
				for (int j = 0; j < voisinsBestSol.size(); j++) {
					index.add(j);
				}

				//Choix d'un voisin aléatoire non tabou et non déjà sélectionné pour la diversification
				do {
					//Si tous les voisins sont tabous, sélection aléatoire pour éviter un blocage
					if (index.size() == 0) {
						random = (int) (Math.random() * voisinsBestSol.size());
						break;
					}
					random = index.remove((int) (Math.random() * index.size()));
				} while (tabou.contains(voisinsBestSol.get(random)));

				solActu = voisinsBestSol.get(random);
				voisinageCourant = solActu.creerVoisinage();
				addTabou(solActu, tailleTabou);
//				System.out.println("Nouvelle solution d'ailleurs : " + solActu.getEspacePerdu() + "(" + cpt + " / " + nbEloignement);
				cpt++;
				if (cpt == nbEloignement) {
					//Fin de la phase de diversification, retour à la phase d'intensification
					diversification = false;
					cpt = 0;
					iter++;
					totalIter++;

					//Mise à jour de la meilleure solution locale
					bestSol = solActu;
				}
			}
		}

		//Temps et nombre d'itérations pour déterminer la solution finale
		bestSolution.setTime(System.currentTimeMillis() - time);
		bestSolution.setNbIter(totalIter);

		return bestSolution;
	}

	/**
	 * Cherche la meilleure solution non tabou dans le voisinage donnée, ou la meilleure solution déjà trouvée.
	 * 
	 * @param voisinage	Voisinage à chercher
	 * @param bestSol	Meilleure solution actuelle
	 * 
	 * @return	La meilleure solution trouvée dans le voisinage ou bestSol si aucune meilleure trouvée
	 */
	private static Solution rechercheMeilleurSolutionDansVoisinage(List<Solution> voisinage, Solution bestSol) {
		Solution meilleurSol = bestSol;
		int i = 0;

		if (voisinage.size() > 0) {
			while (voisinage.get(i).getEspacePerdu() < meilleurSol.getEspacePerdu()) {
				if (!tabou.contains(voisinage.get(i))) {
					meilleurSol = voisinage.get(i);
//					System.out.println("New Best : " + meilleurSol.getEspacePerdu());
				}
				
				i++;
			}
		}

		return meilleurSol;
	}

	/**
	 * Ajout d'une solution à la liste tabou
	 * 
	 * @param sol			Solution à ajouter
	 * @param tailleTabou	Taille de la liste
	 */
	private static void addTabou(Solution sol, int tailleTabou) {
		tabou.add(sol);
		
		//Retrait de la première solution ajoutée en cas de dépassement de la taille
		if (tabou.size() > tailleTabou) {
			tabou.poll();
		}
	}
}
